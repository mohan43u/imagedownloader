package imagedownloader

import (
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

type Site0GalleryPageParser struct {
	tokenizer *html.Tokenizer
	logger *log.Logger
	url string
	galleryid string
	languages []string
	imagepageurls []string
	tags []string
	categories []string
	imagescount int64
}

func NewSite0GalleryPageParser(url string) *Site0GalleryPageParser {
	self := &Site0GalleryPageParser{}
	self.logger = log.Default()
	self.url = url
	return self
}

func GetGalleryId(url string) string {
	fieldsplitter := func(c rune) bool {
		return c == '/'
	}
	galleryids := strings.FieldsFunc(url, fieldsplitter)
	if galleryids[len(galleryids) - 2] == "g" ||
		galleryids[len(galleryids) - 2] == "gallery" {
		return galleryids[len(galleryids) - 1]
	} else {
		return ""
	}
}

func (self *Site0GalleryPageParser) ShouldSplit(token html.TokenType) bool {
	split := false
	switch token {
	case html.TextToken:
		split = true
	case html.StartTagToken:
		nextToken := self.tokenizer.Token()
		if nextToken.Data == "span" {
			split = true
		}
	}
	return split
}

func (self *Site0GalleryPageParser) Parse() bool {
	self.imagepageurls = make([]string, 0)
	self.languages = make([]string, 0)

	response, err := http.Get(self.url)
	if err != nil {
		self.logger.Print("Failed to get", self.url)
		return false
	}

	self.galleryid = GetGalleryId(self.url)
	if len(self.galleryid) <= 0 {
		self.logger.Print("not able to get galleryid", self.url)
		return false
	}

	self.tokenizer = html.NewTokenizer(response.Body)
	loop := true
	for loop {
		switch self.tokenizer.Next() {
		case html.ErrorToken:
			if self.tokenizer.Err() != io.EOF {
				self.logger.Print("Error occurred during parsing: " +
					self.tokenizer.Err().Error())
			}
			loop = false
		case html.StartTagToken:
			token := self.tokenizer.Token()
			for _, attribute := range token.Attr {
				if strings.Compare(attribute.Key, "class") == 0 && strings.Contains(attribute.Val, "pages") {
					switch self.tokenizer.Next() {
					case html.TextToken:
						token := self.tokenizer.Token()
						val := strings.TrimPrefix(token.Data, "Pages: ")
						imagescount, err := strconv.ParseInt(val, 10, 64)
						if err != nil {
							self.logger.Println("failed to parse pages count: ", err)
						} else {
							self.imagescount += imagescount
						}
					}
				}
				if strings.HasPrefix(attribute.Val, "/language/") {
					if self.ShouldSplit(self.tokenizer.Next()) {
						fieldsplitter := func(c rune) bool {
							return c == '/'
						}
						self.languages = append(self.languages, strings.FieldsFunc(attribute.Val, fieldsplitter)[1])
					}
				}

				if strings.HasPrefix(attribute.Val, "/category/") {
					if self.ShouldSplit(self.tokenizer.Next()) {
						fieldsplitter := func(c rune) bool {
							return c == '/'
						}
						self.categories = append(self.categories, strings.FieldsFunc(attribute.Val, fieldsplitter)[1])
					}
				}

				if strings.HasPrefix(attribute.Val, "/tag/") {
					if self.ShouldSplit(self.tokenizer.Next()) {
						fieldsplitter := func(c rune) bool {
							return c == '/'
						}
						self.tags = append(self.tags, strings.FieldsFunc(attribute.Val, fieldsplitter)[1])
					}
				}

				if strings.Contains(attribute.Val, "/" + self.galleryid + "/") {
					self.imagepageurls = append(self.imagepageurls, GetBaseUrl(self.url, false) + attribute.Val)
				}
			}
		}
	}
	return true
}

func (self *Site0GalleryPageParser) GetTags() []string {
	return self.tags
}

func (self *Site0GalleryPageParser) GetLanguages() []string {
	return self.languages
}

func (self *Site0GalleryPageParser) GetCategories() []string {
	return self.categories
}

func (self *Site0GalleryPageParser) GetImagesCount() int64 {
	return self.imagescount
}

func (self *Site0GalleryPageParser) GetImagePageUrls() []string {
	return self.imagepageurls
}

func (self *Site0GalleryPageParser) GetGalleryId() string {
	return self.galleryid
}
