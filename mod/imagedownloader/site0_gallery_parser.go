package imagedownloader

import (
	"fmt"
	"io"
	"log"
	"strings"
	"strconv"
	"net/http"
	neturl "net/url"
	"golang.org/x/net/html"
)

type Site0GalleryParser struct {
	tokenizer *html.Tokenizer
	logger *log.Logger
	gallerypageurls []string
}

func NewSite0GalleryParser() *Site0GalleryParser {
	self := &Site0GalleryParser{}
	self.logger = log.Default()
	return self
}

func GetBaseUrl(url string, withpath bool) string {
	result := ""
	parsedurl, err := neturl.Parse(url)
	if err != nil {
		fmt.Print("Failed to parse url" + url)
		return result
	}
	if withpath == false {
		parsedurl.Path = ""
	}
	parsedurl.RawQuery = ""
	parsedurl.Fragment = ""
	result = parsedurl.String()
	return result
}

func (self *Site0GalleryParser) Parse(url string) string {
	self.gallerypageurls = make([]string, 0)
	lastpagenum := 0
	nexturl := ""

	response, err := http.Get(url)
	if err != nil {
		self.logger.Print("Failed to get ", url)
		return nexturl
	}
	self.tokenizer = html.NewTokenizer(response.Body)
	loop := true
	for loop {
		switch self.tokenizer.Next() {
		case html.ErrorToken:
			if self.tokenizer.Err() != io.EOF {
				self.logger.Print("Error occurred during parsing: " +
					self.tokenizer.Err().Error())
			}
			loop = false
		case html.StartTagToken:
			token := self.tokenizer.Token()
			for _, attribute := range token.Attr {
				if strings.Contains(attribute.Val, "page=") {
					pagenum, _ := strconv.Atoi(strings.Split(attribute.Val, "page=")[1])
					if lastpagenum < pagenum {
						lastpagenum = pagenum
					}
				}
				if strings.HasPrefix(attribute.Val,"/g/") ||
					strings.HasPrefix(attribute.Val, "/gallery/") {
					switch self.tokenizer.Next() {
					case html.TextToken:
						ntoken := self.tokenizer.Token()
						if len(strings.TrimSpace(ntoken.Data)) <= 0 {
							self.gallerypageurls = append(self.gallerypageurls, GetBaseUrl(url, false) + attribute.Val)
						}
					case html.SelfClosingTagToken:
						ntoken := self.tokenizer.Token()
						if ntoken.Data == "img" {
							self.gallerypageurls = append(self.gallerypageurls, GetBaseUrl(url, false) + attribute.Val)
						}
					}
				}
			}
		}
	}
	if lastpagenum > 0 {
		parsedurl,err := neturl.Parse(url)
		if err != nil {
			self.logger.Print("Failed to parse url", err)
		} else {
			query := parsedurl.Query()
			if query.Has("page") {
				pagenum, _ := strconv.Atoi(query.Get("page"))
				if pagenum > 1 && pagenum < lastpagenum {
					query.Set("page", strconv.Itoa(pagenum + 1))
					parsedurl.RawQuery = query.Encode()
					nexturl = parsedurl.String()
				}
			} else {
				pagenum := 2
				if pagenum < lastpagenum {
					query.Set("page", strconv.Itoa(pagenum))
					parsedurl.RawQuery = query.Encode()
					nexturl = parsedurl.String()
				}
			}
		}
	}
	return nexturl
}

func (self *Site0GalleryParser) GetGalleryPageUrls() []string {
	return self.gallerypageurls
}
