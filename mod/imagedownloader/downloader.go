package imagedownloader

import (
	"os"
	"io"
	"log"
	"net/http"
)

type Downloader struct {
	logger *log.Logger
}

func NewDownloader() *Downloader {
	self := &Downloader{}
	self.logger = log.Default()
	return self
}

func (self *Downloader) Mkdir(dir string) bool {
	err := os.MkdirAll(dir, 0755)
	if err != nil {
		self.logger.Println("failed to create", dir, err)
		return false
	}
	return true
}

func (self *Downloader) Download(url string, dir string, filename string) bool {
	if !self.Mkdir(dir) {
		self.logger.Println("Failed to Create Dir", dir)
		return false
	}
	response, err := http.Get(url)
	if err != nil {
		self.logger.Println("Failed to get", url, err)
		return false
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		self.logger.Println("Failed to read response", err)
		return false
	}
	err = os.WriteFile(dir + "/" + filename, data, 0644)
	if err != nil {
		self.logger.Println("Failed to write data", err)
		return false
	}
	return true
}
