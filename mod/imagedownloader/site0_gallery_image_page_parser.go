package imagedownloader

import (
	"io"
	"log"
	"net/http"
	"strings"

	"golang.org/x/net/html"
)

type Site0GalleryImagePageParser struct {
	tokenizer *html.Tokenizer
	logger *log.Logger
	url string
	galleryid string
	imageurls []string
	imageids []string
}

func NewSite0GalleryImagePageParser(galleryid string, url string) *Site0GalleryImagePageParser {
	self := &Site0GalleryImagePageParser{}
	self.logger = log.Default()
	self.url = url
	self.galleryid = galleryid
	return self
}

func getImageId(galleryid string, url string) string {
	fieldsplitter := func(c rune) bool {
		return c == '/'
	}
	imageids := strings.FieldsFunc(url, fieldsplitter)
	if imageids[len(imageids) - 2] == galleryid {
		return imageids[len(imageids) - 1]
	} else {
		return ""
	}
}

func (self *Site0GalleryImagePageParser) Parse() bool {
	self.imageurls = make([]string, 0)
	self.imageids = make([]string, 0)

	response, err := http.Get(self.url)
	if err != nil {
		self.logger.Print("Failed to get", self.url)
		return false
	}

	self.tokenizer = html.NewTokenizer(response.Body)
	loop := true
	for loop {
		switch self.tokenizer.Next() {
		case html.ErrorToken:
			if self.tokenizer.Err() != io.EOF {
				self.logger.Print("Error occurred during parsing: " +
					self.tokenizer.Err().Error())
			}
			loop = false
		default:
			token := self.tokenizer.Token()
			if token.Data == "img" {
				imageid := getImageId(self.galleryid, self.url)
				if len(imageid) <= 0 {
					self.logger.Print("not able to get iamgeid", self.url)
					loop = false
				} else {
					for _, attribute := range token.Attr {
						if attribute.Key == "src" &&
							strings.Contains(attribute.Val, "/" + imageid + ".") {
							self.imageurls = append(self.imageurls, attribute.Val)
							self.imageids = append(self.imageids, imageid)
						}
					}
				}
			}
		}
	}
	return true
}

func (self *Site0GalleryImagePageParser) GetImageUrls() []string {
	return self.imageurls
}

func (self *Site0GalleryImagePageParser) GetImageIds() []string {
	return self.imageids
}
