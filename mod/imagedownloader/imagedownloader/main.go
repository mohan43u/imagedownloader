package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"

	"github.com/akamensky/argparse"
	imagedownloader "gitlab.com/mohan43u/imagedownloader/mod/imagedownloader"
)

func main() {
	var start_url *string
	var offset_url *string
	var tags *[]string
	var categories *[]string
	var languages *[]string
	var minimagescount *int

	argparser := argparse.NewParser("", "show links to download images from a particular website")
	languages = argparser.StringList("l", "language", &argparse.Options{
		Required: false,
		Help:    "language to filter",
		Default: []string{"english"},
	})
	categories = argparser.StringList("c", "category",&argparse.Options{
		Required: false,
		Help: "category to filter",
		Default: []string{"all"},
	})
	offset_url = argparser.String("o", "offseturl", &argparse.Options{
		Required: false,
		Help: "offset url to begin filtering",
	})
	minimagescount = argparser.Int("m", "minimagescount", &argparse.Options{
		Required: false,
		Help: "Minimum images count",
		Default: 0,
	})
	tags = argparser.StringList("t", "tags", &argparse.Options{
		Required: false,
		Help: "tags to filter",
		Default: []string{"all"},
	})
	start_url = argparser.String("u", "url", &argparse.Options{
		Required: true,
		Help: "url",
	})
	err := argparser.Parse(os.Args)
	if err != nil {
		fmt.Fprintln(os.Stderr, "failed to parse cmdline: ", err)
		return
	}

	var offset_galleryid uint64
	if len(*offset_url) != 0 {
		var err error
		offset_galleryid, err = strconv.ParseUint(imagedownloader.GetGalleryId(*offset_url), 10, 64)
		if err != nil {
			fmt.Fprintln(os.Stderr, "invalid offset-url", err)
			return
		}
	}

	url := *start_url
	site0galleryparser := imagedownloader.NewSite0GalleryParser()
	wg := &sync.WaitGroup{}
	for len(url) > 0 {
		fmt.Fprintln(os.Stderr, "processing:", url)
		url = site0galleryparser.Parse(url)
		for _, galleryurl := range site0galleryparser.GetGalleryPageUrls() {
			galleryid, error := strconv.ParseUint(imagedownloader.GetGalleryId(galleryurl), 10, 64)
			if error != nil {
				fmt.Fprintln(os.Stderr, galleryurl, "invalid galleryid", error)
				continue
			}
			if offset_galleryid > 0 && galleryid > offset_galleryid {
				fmt.Fprintln(os.Stderr, galleryurl, "galleryid", galleryid, "less than", offset_galleryid)
				continue
			}

			wg.Add(1)
			func(galleryurl string, wg *sync.WaitGroup) {
				defer wg.Done()

				site0gallerypageparser := imagedownloader.NewSite0GalleryPageParser(galleryurl)
				site0gallerypageparser.Parse()

				language_found := false
				for _, language := range *languages {
					if language == "all" || strings.Contains(strings.Join(site0gallerypageparser.GetLanguages(), ","), language) {
						language_found = true
						break
					}
				}
				if language_found == false {
					fmt.Fprintln(os.Stderr, galleryurl, "required language not found", languages)
					return
				}

				category_found := false
				for _, category := range *categories {
					if category == "all" || strings.Contains(strings.Join(site0gallerypageparser.GetCategories(), ","), category) {
						category_found = true
						break
					}
				}
				if category_found == false {
					fmt.Fprintln(os.Stderr, galleryurl, "required category not found", categories)
					return
				}

				tag_found := false
				for _, tag := range *tags {
					if tag == "all" {
						tag_found = true
						break
					} else {
						page_tags := site0gallerypageparser.GetTags()
						if strings.Contains(tag, ":") == false {
							if strings.Contains(strings.Join(page_tags, ","), tag) {
								tag_found = true
								break
							}
						} else {
							subtags := strings.Split(tag, ":")
							subtags_found_count := 0
							for _, subtag := range subtags {
								if strings.Contains(strings.Join(page_tags, ","), subtag) {
									subtags_found_count += 1
								}
							}
							if subtags_found_count == len(subtags) {
								tag_found = true
								break
							}
						}
					}
				}
				if tag_found == false {
					fmt.Fprintln(os.Stderr, galleryurl, "required tag not found", tags)
					return
				}

				minimagescount_found := false
				imagescount := site0gallerypageparser.GetImagesCount()
				if imagescount < int64(*minimagescount) {
					fmt.Fprintln(os.Stderr, galleryurl, "minimum images count not satisfied", imagescount, *minimagescount)
					return
				} else {
					minimagescount_found = true
				}

				if language_found &&
					category_found &&
					tag_found &&
					minimagescount_found {
					fmt.Println(galleryurl)
				}
			}(galleryurl, wg)
		}
	}
	wg.Wait()
}
