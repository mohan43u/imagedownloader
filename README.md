### Image Downloader

Simple tool to download image files from one particular website

#### Compile

```console
cd mod/imagedownloader/imagedownloader
go build
```

#### Run

```console
cd mod/imagedownloader/imagedownloader
./imagedownloader <site0-baseurl>/tag/<tag-name>
```

This command will download all the images in english for this particular tag,  To know the baseurl, decrypt `sites.enc` file with following command

```console
openssl enc -d -aes-256-ctr -pbkdf2 -a -A -in sites.enc -out sites
```

This `sites.enc` file was created using this command

```console
openssl enc -e -aes-256-ctr -pbkdf2 -in sites -a -A -out sites.enc
```

password: this project's real password
